# tsv笔记

## 简介

tsv是Tab-separated values的缩写，即制表符分隔值。制表符分隔值文件的内容可以包括文本，分为行和列的数学，科学或统计数据。

## tsv的打开方式

1. 在 Excel 中打开 TSV文件
2. 在 Excel 文件菜单上, 单击打开
3. 单击以选中 TSV(为用制表符tab分隔的文件) 或 CSV (为用逗号,分隔的文件)文件
4. 如果文件是逗号分隔, 单击下一步， 然后继续执行步骤
5. 如果文件是制表符分隔，单击完成，然后引用到下文 " 要保存将文件从 Excel 中 TSV 格式 " 部分
6. 分隔符区域中单击以选择逗号，单击以清除所有其他选定分隔符, 然后单击下一步完成。
7. Excel 打开文件, 将每个字段置于单独的列。 可能需要编辑头记录以精确匹配到那些 Outlook 使用作为其内部名称文本数据源中字段名称。 例如, 命名源文件中 " FN " 字段将被作为 " 名称 First " Outlook 无法识别并没有数据导入对于该字段将导致。 需要进行编辑以阅读 " FirstName " 字段名称。

## tsv实操

```
id	name	url
1	摔跤吧！爸爸 Dangal	https://movie.douban.com/subject/26387939/?from=subject-page
2	神秘巨星	                https://movie.douban.com/subject/26942674/?from=subject-page
3	小萝莉的猴神大叔	        https://movie.douban.com/subject/26393561/?from=subject-page
```

## tsv与csv

- tsv与csv最大的区别在于两者的分隔符号。
- TSV 是Tab-separated values的缩写，即制表符分隔值。
- CSV，Comma-separated values（逗号分隔值）。
- TSV是用制表符（Tab,’\t’）作为字段值的分隔符。
- CSV是用半角逗号（’,’）作为字段值的分隔符。

## Python对tsv文件的支持

Python的csv模块准确的讲应该叫做dsv模块，因为它实际上是支持范式的分隔符分隔值文件（DSV，delimiter-separated values）的。 delimiter参数值默认为半角逗号，即默认将被处理文件视为CSV。 当delimiter=’\t’时，被处理文件就是TSV。

## python读取tsv文件

**使用pandas读取tsv文件的代码如下：**

```
train=pd.read_csv('test.tsv', sep='\t')
```

**如果已有表头，则可使用header参数：**

```
train=pd.read_csv('test.tsv', sep='\t', header=0)
```

**如果已有主键列：**

```
train=pd.read_csv('test.tsv', sep='\t', header=0, index_col='id')
```

