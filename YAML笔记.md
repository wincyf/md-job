# YAML笔记

YAML主要用作配置文件，相对操作复杂的XML，YAML显得格外简洁；

对比properties，YAML在层级表示方面同样显得优秀。

YAML中允许表示三种值：**常量** **数组** **对象** 

## 语法要求

YAML对大小写敏感

YAML使用缩进表示层级

YAML不能使用tab缩进，只能使用空格缩进，不限制空格数量，但要求同级左对齐

## 对象

使用冒号代表，格式为key: value。冒号后面要加一个空格

较为复杂的对象格式，可以使用问号加一个空格代表一个复杂的key，配合一个冒号加一个空格代表一个value

该数组的每个元素由三个属性组成：id，name，price

## 常量

YAML中提供了多种常量结构，包括：整数，浮点数，字符串，NULL，日期，布尔，时间。

## 特殊符号

yaml中--表示一个新开始的文件。

--也可以用来分割不同的内容。

... 和---配合使用，在一个配置文件中代表一个文件的结束。

!! YAML中使用!!做类型强行转换。

">"在字符串中折叠换行，| 保留换行符，这两个符号是YAML中字符串经常使用的符号。

引用。重复的内容在YAML中可以使用&来完成锚点定义，使用*来完成锚点引用。

合并内容。主要和锚点配合使用，可以将一个锚点内容直接合并到一个对象中。



在merge中，定义了四个锚点，分别在sample中使用。
sample1中，<<: *CENTER意思是引用{x: 1,y: 2}，并且合并到sample1中，那么合并的结果为：sample1={r=10, y=2, x=1}

sample2中，<<: [*CENTER, *BIG] 意思是联合引用{x: 1,y: 2}和{r: 10}，并且合并到sample2中，那么合并的结果为：sample2={other=haha, x=1, y=2, r=10}

sample3中，引入了*CENTER, *BIG，还使用了r: 100覆盖了引入的r: 10，所以sample3值为：sample3={r=100, y=2, x=1}

有了合并，我们就可以在配置中，把相同的基础配置抽取出来，在不同的子配置中合并引用即可。

*以上示例均通过Snakeyaml测试通过*